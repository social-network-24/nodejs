const {makeExecutableSchema} = require("@graphql-tools/schema");
const express = require("express");
const http = require("http");

const {ApolloServer} = require("apollo-server-express");
const mongoose = require("mongoose");

const typeDefs = require("./graphql/typeDefs");
const resolvers = require("./graphql/resolvers");
const {execute, subscribe} = require("graphql/execution");
const {SubscriptionServer} = require("subscriptions-transport-ws");
require("dotenv").config();

(async function () {

  await mongoose.connect(process.env.MONGODB, {}).then(() => {
    console.log("mongodb connected.");
  }).catch((err) => {
    console.error(err);
  });

  const app = express();
  const httpServer = http.createServer(app);

  const schema = makeExecutableSchema(
    {typeDefs, resolvers}
  );

  const subscriptionServer = SubscriptionServer.create(
    {schema, execute, subscribe}, {
      server: httpServer, path: "/graphql"
    });

  const server = new ApolloServer({
    schema,
    context: ({req}) => ({req}),
    // csrfPrevention: true,
    // cache: "bounded",
    plugins: [{
      async serverWillStart() {
        return {
          async drainServer() {
            subscriptionServer.close();
          }
        };
      }
    }]
  });

  await server.start();
  server.applyMiddleware({app});
  // Now that our HTTP server is fully set up, we can listen to it.
  const PORT = process.env.port || 5000;
  httpServer.listen(PORT, () => {
    console.log(`Server is now running on http://localhost:${PORT}${server.graphqlPath}`);
  });
})();